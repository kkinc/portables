K8S Portable Core Initiative
============================

### Requirements
You need linux box(es) with relatively fresh systemd (240+) 

## etcd

### Images
Portable images for etcd could be downloaded by the following links:
* [etcd_amd64.raw][etcd amd64]
* [etcd_arm64.raw][etcd arm64]
* [etcd_ppc64le.raw][etcd ppc64le]


### Installation
```bash
# Pick the correct image from above
# You need amd64 image in most cases
ETCD_IMAGE="<image url here>"
# Download image to the /etc/portables
sudo curl -L "$ETCD_IMAGE" -o /etc/portables/etcd.raw
# Attach image to systemd
sudo portablectl attach etcd
# Enjoy! You can configure etcd.service and schedule to start it when you want
```

[etcd amd64]: https://gitlab.com/kkinc/portables/-/jobs/artifacts/master/raw/etcd/etcd_3.3.12_amd64.raw?job=etcd
[etcd arm64]: https://gitlab.com/kkinc/portables/-/jobs/artifacts/master/raw/etcd/etcd_3.3.12_arm64.raw?job=etcd
[etcd ppc64le]: https://gitlab.com/kkinc/portables/-/jobs/artifacts/master/raw/etcd/etcd_3.3.12_ppc64le.raw?job=etcd
